## 微服务架构

### 1、项目简介

微服务架构案例核心内容，基于SpringCloud框架几个核心组件，Eureka服务注册与发现组件，Feign声明式的WebService客户端组件，Zuul动态路由网关组件。进行多个数据管理，多个服务管理搭建，多个中间件集成，多业务拆分等模式，搭建SpringCloud微服务框架的综合应用案例。

### 2、技术选型

- 基础层框架：`Spring5+`，`SpringBoot2+`，`SpringCloud2+`
- 持久层框架：`MyBatis`，`MyBatis-Plus`
- 开发组件：`Druid`，`Log4j`，`FastJson`，`JodaTime`，`JavaMail`
- 中间件集成：`RocketMQ`，`Redis`，`Quart`，`ElasticSearch`
- 数据存储：`MySQL`、`Redis`、`ElasticSearch`

### 3、依赖知识点

- `SpringBoot`基础  [GitHub·点这里](https://github.com/cicadasmile/spring-boot-base) 或  [GitEE·点这里](https://gitee.com/cicadasmile/spring-boot-base)
- `SpringBoot`应用  [GitHub·点这里](https://github.com/cicadasmile/middle-ware-parent) 或  [GitEE·点这里](https://gitee.com/cicadasmile/middle-ware-parent)
- `SpringCloud`组件  [GitHub·点这里](https://github.com/cicadasmile/spring-cloud-base) 或  [GitEE·点这里](https://gitee.com/cicadasmile/spring-cloud-base)
- `Linux中间件`搭建  [GitHub·点这里](https://github.com/cicadasmile/linux-system-base) 或  [GitEE·点这里](https://gitee.com/cicadasmile/linux-system-base)

## 文章说明

[微服务架构案例(01)：项目技术选型简介，架构图解说明](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484173&idx=1&sn=0adceb6d5261aa1d59908043ff690423&chksm=fdf457b5ca83dea3203e5901771ba1cb49ce05db5b700c21536111f2e251373a484da123f0c2&token=1855594949&lang=zh_CN#rd)<br/>
[微服务架构案例(02)：业务架构设计，系统分层管理](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484192&idx=1&sn=9327cb5595c3eea1f78f777f37892d18&chksm=fdf45798ca83de8eb4b05b344fcb87ea763d5ba1d880536fc9b422e03033ad7d66217fe6e59e&token=1641533948&lang=zh_CN#rd)<br/>
[微服务架构案例(03)：数据库选型简介，业务数据规划设计](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484199&idx=1&sn=40838d2d9e9c50c35112f03795c4224a&chksm=fdf4579fca83de89ddde900f2b95c4aa1e02edd96d68dd6fb0eef04e314a4214a9e99d9ca1e4&token=1641533948&lang=zh_CN#rd)<br/>
[微服务架构案例(04)：中间件集成，公共服务封装](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484205&idx=1&sn=522b582d6f69d73a89f91881cbd00b55&chksm=fdf45795ca83de83b02b2e4d3ceb0a851b17428c7e14e352040962c77d6bee806c725a0c6a8f&token=1641533948&lang=zh_CN#rd)<br/>
[微服务架构案例(05)：SpringCloud 基础组件应用设计](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484211&idx=1&sn=b18843bfbb9b5b291ff1c8b9626b5c53&chksm=fdf4578bca83de9d2542bd2a71e7d655f5f172f90dde99c931ebc2158bb5dafcf34629dfb308&token=1641533948&lang=zh_CN#rd)<br/>
[微服务架构案例(06)：通过业务、应用、技术、存储方面，聊聊架构](https://mp.weixin.qq.com/s?__biz=MzU4Njg0MzYwNw==&mid=2247484230&idx=1&sn=7cf931b52c280147d41daffee57ec59e&chksm=fdf457feca83dee82ca7385918abae16f7a00a90d74a6d3a06fb9a4ad6ed53d0cf597929385a&token=878984111&lang=zh_CN#rd)<br/>


## 关于作者
【<b>公众号：知了一笑</b>】    【<b>[知乎专栏](https://www.zhihu.com/people/cicadasmile/columns)</b>】<br/>
<img width="255px" height="255px" src="https://avatars0.githubusercontent.com/u/50793885?s=460&v=4"/><br/>

## 推荐项目

|项目名称|GitHub地址|GitEE地址|推荐指数|
|:---|:---|:---|:---|
|SpringCloud微服务架构实战综合案例|[GitHub·点这里](https://github.com/cicadasmile/husky-spring-cloud)|[GitEE·点这里](https://gitee.com/cicadasmile/husky-spring-cloud)|☆☆☆☆☆|
|SpringCloud微服务基础组件案例详解|[GitHub·点这里](https://github.com/cicadasmile/spring-cloud-base)|[GitEE·点这里](https://gitee.com/cicadasmile/spring-cloud-base)|☆☆☆|
|SpringCloud实现分库分表模式下数据库实时扩容|[GitHub·点这里](https://github.com/cicadasmile/cloud-shard-jdbc)|[GitEE·点这里](https://gitee.com/cicadasmile/cloud-shard-jdbc)|☆☆☆☆☆|
|SpringBoot框架基础应用入门到进阶|[GitHub·点这里](https://github.com/cicadasmile/spring-boot-base)|[GitEE·点这里](https://gitee.com/cicadasmile/spring-boot-base)|☆☆☆☆|
|SpringBoot框架整合开发常用中间件|[GitHub·点这里](https://github.com/cicadasmile/middle-ware-parent)|[GitEE·点这里](https://gitee.com/cicadasmile/middle-ware-parent)|☆☆☆☆☆|
|Spring+Mvc框架基础案例详解|[GitHub·点这里](https://github.com/cicadasmile/spring-mvc-parent)|[GitEE·点这里](https://gitee.com/cicadasmile/spring-mvc-parent)|☆☆|
|Java描述常用设计模式,算法,数据结构|[GitHub·点这里](https://github.com/cicadasmile/model-arithmetic-parent)|[GitEE·点这里](https://gitee.com/cicadasmile/model-arithmetic-parent)|☆☆☆☆☆|
|Linux系统基础、运维,常用操作积累|[GitHub·点这里](https://github.com/cicadasmile/linux-system-base)|[GitEE·点这里](https://gitee.com/cicadasmile/linux-system-base)|☆☆☆|